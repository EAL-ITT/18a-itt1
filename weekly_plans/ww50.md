---
Week: 50
Content: Web servers and clients
Material: See links in weekly plan
Initials: MON
---

# Week 50 Web servers and clients

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Have a standard debian 9 vmware VM running.

### Learning goals
* Basic working knowledge about web technology
  * Level 1: Know what common web technology does and how it is commonly used
  * Level 2: Able to set up and debug simple static websites
  * Level 3: Able to set up, configure and debug simple webapps

## Deliverable
* Power point - 5 slides no text
    
    For (any) presentation, pease consider "who is the audience?", "what should they get out of it?"

## Schedule

* 8:15 MON Introduces the curriculum of the day
  
    Exercise 1 - HTTP recap

    On class, we work on webservers - what they are, what they do and such
    Keywords: HTTP, html, css, javascript, REST APIs

* 9:30 Exercise 2

    You willl probably have lots of questions, while starting up.

* 10:30 5 Presentations and discussions on class

* 11:20 MON introduced Ex 3

* 12:15 Exercise 3

* 14:00'ish Presentations on class

    I have a meeting, so I might be a litle delayed.

## Hands-on time

* Exercise 1 (Lvl 1)

  1. Team up with someone from theother end of the room.
  1. Define what a webserver is and what is does. Give 3-5 examples
  2. Define what a web client is and what is does. Give 3-5 examples
  3. Join with another team and compare notes
      Are you in agreement?
  4. Discussion on class, if applicable
  

* Exercise 2 (Lvl 2)

  Install nginx on a debian VM

  1. Use common deb 9 VM. 
  
        Download from [osboxes](https://www.osboxes.org/debian/#debian-9-vmware)
        
        Choose Debian 9.5 and (probably) 64 bit
    
  2. Import it into vmware workstation
  3. Run it and log in
  4. Install nginx

        see [here](https://www.rosehosting.com/blog/how-to-install-and-configure-nginx-on-debian-9/)
        
        Remember to take notes on how you do things, especially if the guide is off or incomplete.
  
  5. Test that it works
  
        Use both command line and GUI

        Use wireshark and save pcaps
        
        refind your connection in the connection logs on the server

  Remember to document your work, so you are able to recrate it later.

  Choose a presentation topic in advance:
  1. What is debian?
  2. VMs i vmware workstation
  2. Sniffin traffic in vmware workstation
  3. What is nginx? 
  4. nginx installation and configuration
  5. Testing the installation

* Exercise 3 (Lvl 3)

  1. Read up on REST APIs (aka. RESTfull APIs). See links below.

  2. Get [this](https://github.com/moozer/restex) working where the virtual machine is the server and your laptop is the client.
      * Use both `index.html` and `connect.py`
      * Use wreshar and save pcap

  3. Describe what is the app is doing using the correct technical terms. 
      Do this using 250 words (and a simple diagram?)

## Comments
* References for RESTfull APIs: 
    * https://www.smashingmagazine.com/2018/01/understanding-using-rest-api/
    * https://www.restapitutorial.com/
